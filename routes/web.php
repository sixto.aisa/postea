<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/','/posts');
Route::redirect('/home','/posts');

#RUTA CON NOMBRE POST,PARA MOSTRAR TODOS LOS POST, SE ASIGNA EL NOMBRE POSTS.INDEX
Route::get('/posts','PostController@index')->name('posts.index');

Route::get('/posts/create','PostController@create');
Route::post('/posts','PostController@store');

#RUTA PARA MOSTRAR SOLO LOS POST DE UN USUARIO
Route::get('/posts/myPosts','PostController@userPosts');
#RUTA PARA ELIMINAR UN POST ENVIADO EL ID DEL POST
Route::delete('/posts/delete/{id}','PostController@destroy');

Route::get('/posts/{id}','PostController@show')->name('post');
Route::get('/postNotificado/{idnotificacion}','PostController@showPostNotificado');
Route::post('/comments','CommentController@store');

Route::get('/user/edit/{id}','EditUserController@edit');
Route::put('/user/update/{id}','EditUserController@update');
Route::get('/user/show/{id}','EditUserController@show');
Route::delete('/user/delete/{id}','EditUserController@destroy');


Auth::routes();


/*
#MUESTRA TODOS LOS POST SIN NINGUN FILTRO
Route::get('/','PostController@index');
#MUESTRA LOS POST QUE HAYAN SIDO CREADOS EL DIA DE HOY
Route::get('/today','PostController@today');
#MUESTRA EL FORMULARIO PARA CREAR NUEVOS POST
Route::view('/posts/create','create');
#INVOCA LA FUNCIÓN CREATE DEL CONTROLADOR POSTCONTROLLER
Route::post('/posts/create','PostController@create');
Route::get('/posts/{id}','PostController@show')->name('post');

Auth::routes();

Route::get('/home', 'PostController@index')->name('home');
*/
