<?php
namespace App\Notifications;
use App\Post;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class UserNotificacion extends Notification
{
    #SE AGREGA EL ATRIBUTO POST Y COMMENT
    protected $post;
    protected $comment;
    #LOS OBJETOS SON RECIBIDOS MEDIANTE EL CONSTRUCTOR
    public function __construct($post,$comment)
    {
        $this->comment = $comment;
        $this->post = $post;
    }
    #SE CAMBIA LA VIA A BASE DE DATOS
    public function via($notifiable)
    {
        return ['database'];
    }
    #IMPLEMENTAMOS LA VIA
    public function toDatabase($notifiable)
    {
        #ALMACENAMOS EL ID DEL POST, SU TITULO Y EL NOMBRE DEL USUARIO QUE COMENTO
        return [
            'post_id' => $this->post->_id,
            'post_title' => $this->post->title,
            'comment_user' =>  $this->comment->user->name
        ];
    }
    public function toArray($notifiable)
    {
        return [ ];
    }
}
