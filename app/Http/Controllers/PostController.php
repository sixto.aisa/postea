<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    public function index()
    {
        //OBTENEMOS TODO LOS POSTS
        #$publicaciones=Post::all();
        $publicaciones=Post::paginate(10);
        //RETORNAMOS LA VISTA INDEX CON LOS POSTS OBTENIDOS
        return view('posts.index',compact('publicaciones'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function showPostNotificado($idnotificacion)
    {
        #IDENTIFICAMOS AL USUARIO DE LA SESION
        $usuario = Auth::user();
        #BUSCAMOS LA NOTIFICACION POR SU ID
        $notificacion = $usuario->notifications()->find($idnotificacion);
        #MARCAMOS LA NOTIFICACION BUSCADA COMO LEIDA
        $notificacion->markAsRead();
        #REDIRECCIONAMOS LA VISTA
        return view('posts.postUnico',['post'=>Post::find($notificacion->data['post_id'])]);
    }

    public function show($id)
    {
        return view('posts.postUnico',['post'=>Post::find($id)]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required:max:120',
            'image'=>'required|image|mimes:jpeg,png,jpg|max:2048',
            'content'=>'required:max:2200',
        ]);
        //$image=$request->file('image');
        //$imageName=time().$image->getClientOriginalName();
        $imageName=$request->file('image')->store('posts/'.Auth::id(),'public');
        $title=$request->get('title');
        $content=$request->get('content');

        $post=$request->user()->posts()->create([
            'title'=>$title,
            //'image'=>'img/'.$imageName,
            'image'=>$imageName,
            'content'=>$content,
        ]);

        //$request->image->move(public_path('img'),$imageName);
        return redirect()->route('post',['id'=>$post->id]);
    }

    public function userPosts(){
        #OBTENEMOS EL USUARIO ATENTICADO
        $user_id=Auth::id();
        #OBTENEMOS LOS POST DE UN USUARIO EN ESPECIFICO
        $publicaciones=Post::where('user_id','=',$user_id)->get();
        #RETORNAMOS LA VISTA MYPOSTS CON LPOST POST OBTENIDOS
        return view('posts.myPosts',compact('publicaciones'));
    }

    public function destroy($id)
    {
        #BUSCAMOS UN POST POR SU ID
        $post=Post::find($id);
        #ELIMINAMOS EL POST ENCONTRADO
        $post->delete();
        #
        return $this->userPosts();
    }

    /*
   public function index()
   {
       $publicaciones=Post::all();
       return view('index',compact('publicaciones'));
   }

   public function today()
   {

       $fechaInicio = Carbon::now()->startOfDay();
       $fechaFin = Carbon::now()->endOfDay();

       #print_r($fechaInicio->toDate());
       #print ("<br>");
       #print_r($fechaFin->toDate());

       $publicaciones=Post::whereBetween('created_at',[$fechaInicio,$fechaFin])->get();
       return view('today',compact('publicaciones'));
   }

   public function show($id)
   {
       $resultado=Post::find($id);
       return view('postUnico',['post'=>$resultado]);
   }

   public function create(Request $request)
   {
       $request->validate([
           'title'=>'required:max:120',
           'image'=>'required|image|mimes:jpeg,png,jpg|max:2048',
           'content'=>'required:max:2200',
           ]);

       $image=$request->file('image');
       $imageName=time().$image->getClientOriginalName();
       $title=$request->get('title');
       $content=$request->get('content');

       $post=new Post();
       $post->title=$title;
       $post->image='img/'.$imageName;
       $post->content=$content;
       $post->save();

       $request->image->move(public_path('img'),$imageName);
       return redirect()->route('post',['id'=>$post->id]);
   }
   */

}
