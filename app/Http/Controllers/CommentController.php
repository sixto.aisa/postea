<?php

namespace App\Http\Controllers;

use App\Notifications\UserNotificacion;
use Illuminate\Http\Request;
use App\Post;
use App\Comment;

class CommentController extends Controller
{
    public function __construct()
    {
        #Le indicamos a Laravel que sólo permita a los usuarios autorizados
        #accedan al controlador
        $this->middleware('auth');
    }

    #FUNCION PARA CREAR NUEVOS COMENTARIOS
    public function store(Request $request)
    {
        #VALIDACION DE 250 CARACTERES COMO MÁXIMO PARA LOS COMENTARIOS
        $request->validate([
           'content'=>'required:max:250',
        ]);

        #SE CREA UNA INSTANCIA DE LA CLASE COMMENT
        $comment=new Comment();
        #SE ASIGNA EL ID DEL USUARIO DE LA SESIÓN ACTUAL
        $comment->user_id=$request->user()->id;
        #SE ASIGNA EL CONTENIDO DEL COMENTARIO
        $comment->content=$request->get('content');
        #SE BUSCA EL OBJETO DEL POST QUE SE ESTA COMENTANDO
        $post=Post::find($request->get('post_id'));
        #SE REGISTRA EL COMENTARIO
        $post->comments()->save($comment);
        #SE OBTIENE EL USUARIO QUE CREO EL POST
        $user=$post->user;
        #SE NOTIFICA AL USUARIO PANSANDO COMO ARGUMENTOS DEL
        #CONSTRUCTOR EL OBJETO POST Y COMMENT
        $user->notify(new UserNotificacion($post,$comment));
        #SE REDIRIGE A LA MISMA PAGINA PARA MOSTRAR EL COMENTARIO CREADO
        return redirect()->route('post',['id'=>$request->get('post_id')]);
    }
}
