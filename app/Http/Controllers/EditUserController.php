<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class EditUserController extends Controller
{
    public function __construct()
    {
        #PARA USAR LOS METODOS DEL CONTROLADOR
        #LOS USUARIOS DEBEN INICIAR SESIÓN
        $this->middleware('auth');
    }

    public function edit($user_id)
    {
        #BUSCAMOS UN USUARIO POR SU ID
        $user= User::find($user_id);
        #RETORNAMOS LA VISTA EDIT CON EL USUARIO BUSCADO
        return view('auth.edit',compact('user'));
    }

    public function update(Request $request,$user_id)
    {
        #VALIDAMOS LA INFORMACION DEL FORMULARIO
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        #RECUPERAMOS LOS DATOS ENVIADOS DESDE EL FORMULARIO
        $name=$request->get('name');
        $email=$request->get('email');

        #BUSCAMOS UN USUARIO POR SU ID
        $user= User::find($user_id);
        #ASIGNAMOS LOS VALORES OBTENIDOS EN EL OBJETO USER
        $user->name=$name;
        $user->email=$email;
        #ACTUALIZAMOS EL USUARIO
        $user->update();
        #RETORNAMOS A LA VISTA DE TODOS LOS POST
        return redirect('/posts');
    }

    public function show($user_id)
    {
        #BUSCAMOS UN USUARIO POR SU ID
        $user= User::find($user_id);
        #MOSTRAMOS LA VISTA DELETE CON EL USUARIO BUSCADO
        return view('auth.delete',compact('user'));
    }

    public function destroy($user_id)
    {
        #BUSCAMOS UN USUARIO POR SU ID
        $user= User::find($user_id);
        #OBTENEMOS TODOS LOS POSTS DEL USUARIO BUSCADO
        $posts=$user->posts;
        #REALIZAMOS UN BUCLE POR LOS POSTS PUBLICADOS DE CADA USUARIO
        $posts->each(function ($post){
            #OBTENEMOS LOS COMENTARIOS DE CADA POST
            $comments=$post->comments;
            #REALIZAMOS UN BUCLE DE LOS COMENTARIOS
            $comments->each(function ($comment){
                #ELIMINAMOS UN COMENTARIO
                $comment->delete();
            });
           #ELIMINAMOS UN POST
           $post->delete();
        });

        #DESPUES DE ELIMINAR LOS POST Y COMENTARIOS
        #ELIMINAMOS AL USUARIO
        $user->delete();
        #REDIRIGIMOS A LA VISTA PRINCIPAL
        return redirect()->route('posts.index');
    }
}
