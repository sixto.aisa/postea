<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Post extends Model
{
    protected $fillable=[
        'title','image','content'
    ];

    #SEA ESTABLECIDO UNA RELACIÓN DE UNO A UNO CON EL MODELO USER
    #UN POST TIENE UN USUARIO
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    #SEA ESTABLECIDO UNA RELACIÓN DE UNO A MUCHOS CON EL MODELO COMMENT
    #UN POST TIENE MUCHOS COMENTARIOS
    public function comments()
    {
        return $this->embedsMany('App\Comment');
    }

}
