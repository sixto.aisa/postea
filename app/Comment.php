<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Comment extends Model
{
    #SEA DETERMINADO EL CAMPO PARA SU CREACIÓN
    protected $fillable=[
        'content',
    ];

    #SEA ESTABLECIDO UNA RELACIÓN DE UNO A UNO CON EL MODELO USER
    #UN COMENTARIO TIENE UN USUARIO
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    #SEA ESTABLECIDO UNA RELACIÓN DE UNO A UNO CON EL MODELO POST
    #UN COMENTARIO PERTENECE A UN POST
    public function post()
    {
        return $this->belongsTo('App\Post');
    }
}
