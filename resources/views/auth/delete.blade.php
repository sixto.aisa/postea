<!--SE EXTIENDE DE LA PLANTILLA PRINCIPAL APP.BLADE.PHP -->
@extends('layouts.app')
<!--SE DECLARA LA SECCION DEL CONTENIDO -->
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('ELIMINAR') }}</div>
                <div class="card-body">
                    <!--FORMULARIO PARA EDITAR LOS DATOS DE UN USUARIO
                    SE UTILIZA EL METODO POST Y EL METODO UPDATE DEL CONTROLADOR-->
                    <form method="post" action="{{action('EditUserController@destroy',$user->_id)}}"  enctype="multipart/form-data">
                        @csrf<!-- SE UTILIZA UN TOKEN DE SEGURIDAD -->
                        @method('DELETE')<!-- VERBO UTILIZADO EN EL ROUTE -->
                        <div class="form-group row">
                            <!-- NOMBRE DEL USUARIO -->
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre:') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{$user->name}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <!-- CORREO ELECTRONICO -->
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo Electrónico:') }}</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{$user->email}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <!-- BOTON PARA ELIMINAR UN  -->
                                <button type="submit" class="btn btn-danger">
                                    {{ __('ELIMINAR') }} <i class="fas fa-trash-alt icono-blanco"></i>
                                </button>
                                <!-- BOTON PARA REGRESAR AL LISTADO DE POSTS  -->
                                <button type="button" class="btn btn-secondary" onclick="window.location='{{ route("posts.index") }}'">
                                    {{ __('CANCELAR') }} <i class="fas fa-undo-alt icono-blanco"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
