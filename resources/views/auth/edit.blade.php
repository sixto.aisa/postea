<!--SE EXTIENDE DE LA PLANTILLA PRINCIPAL APP.BLADE.PHP -->
@extends('layouts.app')
<!--SE DECLARA LA SECCION DEL CONTENIDO -->
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('EDITAR') }}</div>
                <div class="card-body">
                    <!--FORMULARIO PARA EDITAR LOS DATOS DE UN USUARIO
                     SE UTILIZA EL METODO POST Y EL METODO UPDATE DEL CONTROLADOR-->
                    <form method="post" action="{{action('EditUserController@update',$user->_id)}}"  enctype="multipart/form-data">
                        @csrf<!-- SE UTILIZA UN TOKEN DE SEGURIDAD -->
                        @method('PUT')<!-- VERBO UTILIZADO EN EL ROUTE -->
                        <div class="form-group row">
                            <!-- NOMBRE DEL USUARIO -->
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre:') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$user->name}}" required autocomplete="name" autofocus>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <!-- CORREO ELECTRONICO -->
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo Electrónico:') }}</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$user->email}}" required autocomplete="email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <!-- PASSWORD -->
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Clave:') }}</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- CONFIRMAR PASSWORD -->
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Clave:') }}</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group row mb-0">

                            <div class="col-md-6 offset-md-4">
                                <!-- BOTON PARA ACTUALIZAR LOS DATOS DEL USUARIO -->
                                <button type="submit" class="btn btn-warning">
                                    {{ __('ACTUALIZAR') }} <i class="fas fa-save icono"></i>
                                </button>
                                <!-- BOTON PARA REGRESAR AL LISTADO DE POSTS  -->
                                <button type="button" class="btn btn-secondary" onclick="window.location='{{route("posts.index") }}'">
                                    {{ __('CANCELAR') }} <i class="fas fa-undo-alt icono-blanco"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
