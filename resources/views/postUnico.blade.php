<!--SE EXTIENDE DE LA PLANTILLA PRINCIPAL APP.BLADE.PHP -->
@extends('layouts.app')
<!--SE DECLARA LA SECCION DEL CONTENIDO -->
@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-8">
                <div class="card">

                    <!--SE MUESTRA EL TITULO DE LA PUBLICACION -->
                    <h2 class="card-title">{{$post->title}}</h2>
                    <!--SE MUESTRA LA IMAGEN DE LA PUBLICACION MEDIANTE EL HELPER ASSET -->
                    <img src="{{asset($post->image)}}" class="card-img-top" STYLE="width: 70%" alt="...">

                    <div class="card-body">
                        <!--SE MUESTRA LA FECHA DE CREACIÓN DEL POST -->
                        <h6 class="card-subtitle mb-2 text-muted">{{$post->created_at}}</h6>
                        <p class="card-text">{{$post->content}}</p>

                        <!--SI EL USUARIO ESTA AUTENTICADO SE MUESTRA EL FORMULARIO PARA ENVIAR COMENTARIOS -->
                        @auth
                            <form>
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea class="form-control" id="txtComentario" rows="4"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10">
                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-dark">Enviar</button>
                                    </div>
                                </div>
                            </form>
                        @endauth

                        <!--SECCION DE COMENTARIOS   -->
                        <h5 > {{ __('Comentarios') }}</h5>

                        <!-- PRIMER COMENTARIO   -->
                        <div class="row">
                            <div class="col-md-12">
                                {{__('Este es el primer comentario del blog.')}}
                            </div>
                        </div>
                        <!-- USUARIO DEL PRIMER COMENTARIO   -->
                        <div class="row">
                            <div class="col-md-8"></div>
                            <div class="col-md-4 text-right">
                                <i class="fas fa-user-alt icono"></i> {{ __('Carlos Ohashi') }}
                            </div>
                        </div>

                        <!-- SEGUNDO COMENTARIO   -->
                        <div class="row">
                            <div class="col-md-12">
                                {{__('Este es el segundo comentario del blog.')}}
                            </div>
                        </div>
                        <!-- USUARIO DEL SEGUNDO COMENTARIO   -->
                        <div class="row">
                            <div class="col-md-8"></div>
                            <div class="col-md-4 text-right">
                                <i class="fas fa-user-alt icono"></i> {{ __('Jordy Carita') }}
                            </div>
                        </div>
                        <!--LINK PARA REGRESAR A LA PGINA PRINCIPAL USANDO NUESTRO CONTROLADOR -->
                        <a href="{{action('PostController@index',$post->id)}}" class="card-link">
                            Todas las publicaciones
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection





