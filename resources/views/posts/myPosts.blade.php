<!--SE EXTIENDE DE LA PLANTILLA PRINCIPAL APP.BLADE.PHP -->
@extends('layouts.app')
<!--SE DECLARA LA SECCION DEL CONTENIDO -->
@section('content')
    <div class="container">
        <!--SE REALIZA UN BUCLE PARA MOSTRAR LAS PUBLICACIONES -->
        @foreach($publicaciones  as $publicacion)
            <div class="row mb-4 justify-content-md-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="row justify-content-center align-items-center">
                                <div class="col-9 subtitulo">
                                    <h5 class="card-title">
                                        <!--SE MUESTRA UN ENLACE PARA VER EL DETALLE DE LA PUBLICACION -->
                                        <a href="{{action('PostController@show',$publicacion->id)}}">
                                            <!--SE MUESTRA EL TITULO DE LA PUBLICACION -->
                                            {{$publicacion->title}}
                                        </a>
                                    </h5>
                                </div>
                                <div class="col-3">
                                    <!--SE MUESTRA EL BOTON PARA ELIMINAR LA PUBLICACION
                                    SE UTILIZA EL METODO POST -->
                                    <form action="{{action('PostController@destroy',$publicacion->id)}}" method="post" enctype="multipart/form-data" novalidate>
                                        @csrf <!--TOKEN DE SEGURIDAD -->
                                        @method('DELETE')<!-- VERBO UTILIZANDO EN EL ROUTE -->
                                        <button type="submit" class="btn btn-danger">Eliminar <i class="fas fa-trash-alt icono-blanco"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--SE MUESTRA LA IMAGEN DE LA PUBLICACION   -->
                        <!-- <img src="{{asset($publicacion->image)}}" class="card-img-top" alt="..."> -->
                        <img src="{{Storage::url($publicacion->image)}}" class="card-img-top" >
                        <div class="card-footer text-muted">
                            <!--SE MUESTRA EL USUARIO QUE CREO LA PUBLICACION   -->
                            Publicado por {{$publicacion->user->name}}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
