<!--SE EXTIENDE DE LA PLANTILLA PRINCIPAL APP.BLADE.PHP -->
@extends('layouts.app')
<!--SE DECLARA LA SECCION DEL CONTENIDO -->
@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-8">
                <div class="card">
                    <!--SE MUESTRA LA IMAGEN DE LA PUBLICACION -->
                    <!-- <img src="{{asset($post->image)}}" class="card-img-top" STYLE="width: 70%" alt="..."> -->
                    <img src="{{Storage::url($post->image)}}" class="card-img-top" STYLE="width: 70%" alt="...">

                    <div class="card-body">
                        <!--SE MUESTRA EL TITULO DE LA PUBLICACION -->
                        <h3 class="card-title">{{$post->title}}</h3>
                        <!--SE MUESTRA LA FECHA DE CREACIÓN DEL POST -->
                        <h6 class="card-subtitle mb-2 text-muted">{{$post->created_at->toFormattedDateString()}}</h6>
                        <!--SE MUESTRA EL CONTENIDO DEL POST -->
                        <p class="card-text">{{$post->content}}</p>
                        <!--LINK PARA REGRESAR A LA PGINA PRINCIPAL USANDO NUESTRO CONTROLADOR -->
                        <a href="{{action('PostController@index')}}" class="card-link">
                            Todas las publicaciones
                        </a>

                        <!--SI EL USUARIO ESTA AUTENTICADO SE MUESTRA EL FORMULARIO PARA ENVIAR COMENTARIOS -->
                        @auth
                            <!--EL FORMULARIO UTILIZA EL CONTROLADOR COMMENTCONTROLLER POR EL METODO POST -->
                            <form action="{{action('CommentController@store',['post_id'=>$post->id])}}" method="post" enctype="multipart/form-data">
                                <!-- TOKEN DE SEGURIDAD -->
                                @csrf
                                <div class="form-group">
                                    <label class="col-sm-2 col-form-label" for="content">{{__('Comment')}}</label>

                                    <div class="col-sm-12">
                                        <!-- VALIDACION DEL VALOR DE COMMENT -->
                                        <textarea class="form-control @error('content') is-invalid @enderror"
                                                  id="content" name="content" rows="3">{{old('content')}}</textarea>

                                        <!-- SI HUBIERA UN ERROR SE MUESTRA UNA ADVERTENCIA -->
                                        @error('content')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{$message}}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <!-- BOTON PARA REGISTRAR EL FORMULARIO -->
                                        <button type="submit" class="btn btn-dark">{{__('Crear')}}</button>
                                    </div>
                                </div>
                            </form>
                        @endauth

                        <!--SI EL USUARIO ES UN INVITADO LO INVITAMOS A QUE SE REGISTRE O INICIE SESION -->
                        @guest
                            <p>Si deseas comentar
                                <!--LO REDIRIGIMOS PARA QUE INICIE SESION -->
                                <a href="{{action('Auth\LoginController@showLoginForm')}}">Iniciar Sesión</a>o
                                <!--LO REDIRIGIMOS PARA QUE SE REGISTRE -->
                                <a href="{{action('Auth\RegisterController@showRegistrationForm')}}">Registrarse</a>
                            </p>
                        @endguest

                        <!--SE REALIZA UN BUCLE PARA MOSTRAR LOS COMENTARIOS DEL POST -->
                        @forelse ($post->comments as $comment)
                            <div class="card">
                                <div class="card-body">
                                    <!--SE MUESTRA EL USUARIO QUE REALIZÓ EL COMENTARIO -->
                                    <h5 class="card-title">{{$comment->user->name}}</h5>
                                    <!--LA FECHA EN FORMATO MES DIA AÑO -->
                                    <h6 class="card-subtitle mb-2 text-muted">{{$comment->created_at->toFormattedDateString()}}</h6>
                                    <!--EL COMENTARIO -->
                                    <p class="card-text">{{$comment->content}}</p>
                                </div>
                            </div>
                        <!--SI NO EXISTEN COMENTARIOS INVITAMOS AL USUARIO QUE REALICE UNO -->
                        @empty
                            <p>No hay comentarios para esta publicación, se el primero</p>
                        @endforelse

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection





