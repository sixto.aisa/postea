@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <h2>Nueva publicación</h2>
        </div>
        <div class="row justify-content-center">
            <!-- SE VERIFICA SI LA VARIABLE $ERRORS TIENE ELEMENTOS
             LO QUE DETERMUNA QUE HAY ERRORES-->
            @if(count($errors)>0)
                <!-- SI HUBIERA ERRORES SE MUESTRA UNA LISTA DESORDENADA
                     EN LA CUAL SE MUESTRA LOS ERRORES ENCONTRADOS   -->
                <div class="row alert alert-danger">
                    <p>¡Opss! Hubo problemas con los datos proporcionados</p>
                    <ul>
                        <!--SE REALIZA UN BUCLE PARA MOSTRAR LOS ERRORES DE LA
                         COLLECION $ERRORS-->
                        @foreach($errors->all() as $error)
                            <!-- SE MUESTRA EL ERROR  -->
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- SE CREA UN FORMULARIO QUE UTILIZARA EL METODO POST
                 PARA LA ACCION DEL FORMULARIO SE UTILIZA EL HELPERS ACTION EL CUAL INDICA QUE SE UTILIZARÁ
                 EL METODO CREATE DE CONTROLADOR POSTCONTROLLER-->
            <form action="{{action('PostController@store')}}" method="post" enctype="multipart/form-data">
                <!-- TOKEN DE SESIÓN PARA EL FORMULARIO -->
                @csrf
                <div class="form-group">
                    <label class="col-sm-2 col-form-label" for="title">{{__('Title')}}</label>
                    <div class="col-sm-12">
                        <!-- SE VALIDA SI TITLE SE ENCUENTRA DENTRO DEL ARRAY $ERRORS
                             SI ES EL CASO SE AGREGA LA CLASE IS-INVALID PARA MOSTRAR EL ERROR-->
                        <!-- SE UTILIZA EL HELPERS OLD PARA OBTENER EL ANTIGUO VALOR DE SESIÓN DE TITLE -->
                        <input id="title" class="form-control{{$errors->has('title') ? ' is-invalid' : ''}}"
                        type="text" name="title" value="{{old('title')}}" autofocus>
                        <!-- SI TITLE SE ENCUENTRA EN EL ARRAY $ERRORS -->
                        @if($errors->has('title'))
                            <span class="invalid-feedback">
                                <!-- SE MUESTRA LA PRIMERA COINCIDENCIA DE TITLE  -->
                                <strong>{{$errors->first('title')}}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-form-label" for="content">
                        {{__('Content')}}
                    </label>
                    <div class="col-sm-12">
                        <textarea class="form-control{{$errors->has('content') ? ' is-invalid':''}}"
                                  id="content" name="content" rows="3">{{old('content')}}</textarea>
                        @if($errors->has('content'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('content')}}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input{{$errors->has('image') ? ' is-invalid' :'' }}"
                                    id="image" name="image">
                            <label class="custom-file-label" for="customFile">
                                {{__('Choose image')}}
                            </label>
                            @if($errors->has('image'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$errors->first('image')}}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary">
                            {{__('Create')}}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
