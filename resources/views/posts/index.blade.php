<!--SE EXTIENDE DE LA PLANTILLA PRINCIPAL APP.BLADE.PHP -->
@extends('layouts.app')
<!--SE DECLARA LA SECCION DEL CONTENIDO -->
@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-9 subtitulo">
            </div>
            <div class="col-3">
                @auth
                <!--SI EL USUARIO ESTA AUTENTICADO SE MUESTRA UN LINK PARA VER SUS PROPIAS PUBLICACIONES -->
                    <a class="btn btn-success" href="{{action('PostController@userPosts')}}">
                        Ver Mis Publicaciones <i class="fas fa-search icono-blanco"></i>
                    </a>
                @endauth
            </div>
        </div>
        <!--SE REALIZA UN BUCLE PARA MOSTRAR LAS PUBLICACIONES -->
        @foreach($publicaciones  as $publicacion)
            <div class="row mb-4 justify-content-md-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">
                                <!--SE MUESTRA UN ENLACE PARA VER EL DETALLE DE LA PUBLICACION -->
                                <a href="{{action('PostController@show',$publicacion->id)}}">
                                    <!--SE MUESTRA EL TITULO DE LA PUBLICACION -->
                                    {{$publicacion->title}}
                                </a>

                            </h5>
                        </div>
                        <!--SE MUESTRA LA IMAGEN DE LA PUBLICACION  -->
                        <!-- <img src="{{asset($publicacion->image)}}" class="card-img-top" alt="..."> -->
                        <!-- <img src="{{Storage::url($publicacion->image)}}" class="card-img-top" alt="..."> -->
                        <img src="{{asset('storage'.'/'.$publicacion->image)}}" class="card-img-top">

                        <div class="card-footer text-muted">
                            Publicado por : {{$publicacion->user->name}}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <!--SE AGREGA UNA PAGINACIÓN -->
        {{$publicaciones->links()}}
    </div>
@endsection
