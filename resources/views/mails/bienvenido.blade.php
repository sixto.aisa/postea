@component('mail::message')
# Bienvenido
<h1>¡ Felicidades ! {{$user->name}}</h1>
Ahora Ud. es parte de nuestra comunidad y podrá compartir
todas su publicaciones en nuestra plataforma.
@endcomponent

