<!--SE EXTIENDE DE LA PLANTILLA PRINCIPAL APP.BLADE.PHP -->
@extends('layouts.app')

<!--SE DECLARA LA SECCION DEL CONTENIDO -->
@section('content')
    <div class="container">
        <!--SE REALIZA UN BUCLE PARA MOSTRAR LAS PUBLICACIONES -->
        @foreach($publicaciones  as $publicacion)
            <div class="row mb-4 justify-content-md-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">
                                <!--SE MUESTRA UN ENLACE PARA VER EL DETALLE DE LA PUBLICACION HACIENDO
                                USO DE LA VARIABLE #PUBLICACION -->

                                <a href="{{action('PostController@show',$publicacion->id)}}">
                                    <!--SE MUESTRA EL TITULO DE LA PUBLICACION -->
                                    {{$publicacion->title}} -   {{$publicacion->created_at}}
                                </a>
                            </h5>
                        </div>
                        <!--SE MUESTRA LA IMAGEN DE LA PUBLICACION MEDIANTE LA RUTA ALMACENADA,
                          QUE ES PASADA AL HELPER ASSET-->
                        <img src="{{asset($publicacion->image)}}" class="card-img-top" alt="...">
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
