<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- SCRIPTS  -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- FUENTES UTILIZANDO CDN -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- ESTILOS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <!-- SE MUESTRA UN NAVBAR CON EL ESTILO DARK DE BOOTSTRAP -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark navbar-fixed-top shadow-sm">
            <!-- CONTENEDOR -->
            <div class="container">

                    <a class="navbar-brand" href="{{ url('/') }}">
                        <!-- SE MUESTRA EL NOMBRE DE LA APLICACIÓN -->
                        <h1><i>{{config('app.name', 'Laravel') }}</i></h1>
                    </a>

                <!-- SE MUESTRA EL BOTON DE MENU PARA PANTALLA PEQUEÑAS -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>


                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- SI EL USUARIO ES UN INVITADO-->
                        @guest
                            <!-- SE MUESTRA EL LINK PARA AUTENTICARSE -->
                            <li class="nav-item">
                                <!-- SE REFERENCIA EL LINK CON LA VISTA LOGIN CON EL NOMBRE INGRESAR-->
                                <a class="nav-link" href="{{ route('login') }}">
                                    <h4>{{ __('Ingresar') }}</h4>
                                </a>
                            </li>
                                <!-- SE MUESTRA EL LINK PARA REGISTRARSE -->
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <!-- SE REFERENCIA EL LINK CON LA VISTA REGISTER CON EL NOMBRE REGISTRARSE-->
                                    <a class="nav-link" href="{{ route('register') }}">
                                        <h4>{{ __('Registrarse') }}</h4>
                                    </a>
                                </li>
                            @endif
                        @else
                                <!-- SI EL USUARIO ESTA AUTENTICADO -->
                                <!-- SE VERIFICA SI TIENE NOTIFICACIONES  -->
                                {{$notificacionesSinLeer=count(Auth::user()->unreadNotifications)}}
                                @if($notificacionesSinLeer>0)
                                    <li class="nav-item dropdown active">
                                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                            <!-- SE MUESTRA EL ICONO DE CAMPANA Y EL NÚMERO DE NOTIFICAIONES SIN LEER -->
                                            <h5><i class="fas fa-bell icono"></i>
                                                {{$notificacionesSinLeer}}
                                            </h5>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                            <!-- SE REALIZA UN BUCLE PARA MOSTRAR LAS NOTIFICAIONES SIN LEER -->
                                        @foreach (Auth::user()->unreadNotifications as $notificacion)
                                            <!-- SE LLAMA AL METODO SHOWPOSTNOTIFICADO PARA MARCAR COMO LEIDA LA NOTIFICACION -->
                                                <a class="dropdown-item"  href="{{action('PostController@showPostNotificado',$notificacion->_id)}}"  >
                                                    <!-- SE MUESTRA EL NOMBRE DEL USUARIO Y EL TITULO DEL POST -->
                                                    <i>{{$notificacion->data["comment_user"] }}</i> a comentado en
                                                    <b>{{ $notificacion->data["post_title"] }}</b>
                                                </a>
                                            @endforeach
                                        </div>
                                    </li>
                                @endif

                                <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <h5> <i class="fas fa-user-alt icono"></i>  {{ Auth::user()->name }} </h5>
                                </a>

                                <!-- SE MUESTRA UN LINK PARA EDITAR DATOS DEL USUARIO -->
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{action('EditUserController@edit',Auth::user()->id)}}" >
                                        {{ __('EDITAR') }}
                                    </a>

                                    <a class="dropdown-item" href="{{action('EditUserController@show',Auth::user()->id)}}" >
                                        {{ __('ELIMINAR') }}
                                    </a>

                                    <div class="dropdown-divider"></div>

                                    <!-- SE MUESTRA UN LINK PARA SALIR DE LAS SESION -->
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('SALIR') }}
                                    </a>

                                    <!-- SE ENVIA EL FORMULARIO UTILIZANDO EL METODO POST PARA SALIR DE LA APLICACIÓN -->
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        <!-- SE UTILIZA UN TOKEN DE SEGURIDAD -->
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
