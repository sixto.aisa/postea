<?php
use Illuminate\Database\Seeder;
use App\User;
class UserCollectionSeeder extends Seeder
{
    public function run()
    {
        #SE CREAN 10 USUARIOS DE PRUEBA
        $users=factory(User::class,1)->create()->each(function ($user){
            #POR CADA USUARIO SE CREAN 5 POSTS
            $user->posts()->createMany(
                factory(\App\Post::class,5)->make()->toArray()
            );
        });

        #SE CREA UN USUARIO PREDETERMINADO
        User::create([
                'name' => 'demo',
                'email' => 'demo@tecsup.edu.pe',
                'email_verified_at' => now(),
                'password' =>Hash::make('123456'),
                'remember_token' => Str::random(10),
            ])->posts()->createMany(
            #SE CREA 5 POST PARA EL USUARIO PREDETERMINADO
            factory(\App\Post::class,5)->make()->toArray()
        );
    }
}
