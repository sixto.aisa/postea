<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;

$factory->define(Post::class, function (Faker $faker) {
    #SE OBTIENE LA RUTA PUBLICA DE LA APLICACIÓN POSTEA
    $filepath = public_path() ;
    #SE SOLICITA UNA IMAGEN DE PRUEBA LA CUAL ES DESCARGADA EN LA RUTA PUBLICA
    # $faker->image, SOLO DEVUELVE EL NOMBRE DEL ARCHIVO
    $image= $faker->image($filepath,300,300,null,false);

    return [
        'title'=>$faker->sentence(5),#se requiere un titulo de 5 palabras
        'content'=>$faker->paragraph(3),#3 parrafos
        #ALMACENAMOS LA IMAGEN DESCARGADA EN NUESTRO DISCO PUBLICO DENTRO DE LA CARPETA POST
        #Storage::disk, DEVUELVE LA RUTA RELATIVA DONDE SE ALMACENA LA IMAGEN
        'image'=>Storage::disk('public')->putFile('post',$filepath."/".$image),
    ];
});
