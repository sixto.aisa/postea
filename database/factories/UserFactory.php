<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
#'email' => $faker->unique()->safeEmail,

$factory->define(User::class, function (Faker $faker) {
    #SE ASIGNA LA MISMA CONTRASEÑA A TODOS LOS USUARIOS
    $hashed_password = Hash::make('123456');
    #SE USA EL MISMO DOMINIO PARA TODOS LOS USUARIOS
    $domain = 'tecsup.edu.pe';
    return [
        #OBTENEMOS EL NOMBRE DE USUARIO
        $userName = $faker->userName,
        'name' => $userName,
        #CONSTRUIMOS EL CORREO CON EL NOMBRE DE USUARIO Y EL DOMINIO ASIGNADO
        'email' => $userName."@".$domain,
        'email_verified_at' => now(),
        'password' => $hashed_password,
        'remember_token' => Str::random(10),
    ];
});
